const fs = require("fs");
const yargs = require("yargs/yargs");
const { hideBin } = require("yargs/helpers");
const argv = yargs(hideBin(process.argv)).argv;

const { dashboardAPIWrapper } = require("./controllers/dashboard");

const Init = () => {
  console.log("excecuting api wrapper function...");
  if (argv.filePath) {
    const rawData = fs.readFileSync(argv.filePath);
    const { resourceType, method, body } = JSON.parse(rawData);

    switch (resourceType) {
      case "dashboard":
        dashboardAPIWrapper(method, body);
        break;
      default:
        break;
    }
  }
  console.log("resource created successfully !!!");
};

Init();
