const axios = require("axios");

const instance = axios.create({
  headers: {
    "Content-Type": "application/json",
    "DD-API-KEY": "2166eeb2a6ccff659d3d733709128ab1",
    "DD-APPLICATION-KEY": "02565b2468b5b6749dd17cc63a04ce6333d87946",
  },
});

const dashboardAPIWrapper = async(method, body) => {
  switch (method) {
    case "POST":
      const res = await instance.post("https://api.datadoghq.com/api/v1/dashboard", {
        ...body,
      });
      break;
    default:
      break;
  }
};

module.exports = {
  dashboardAPIWrapper,
};
